package scilaf.examples;

import java.util.Arrays;
import java.util.Hashtable;

import scilaf.framework.BayesianInference;
import smile.Network;
import smile.learning.DataSet;

public class TestBayesianInference {	
	public static void main(String[] args){
		BayesianInference instance = new BayesianInference();
		String ws = "E:/workspace/tmp/";
		String model = ws + "bayesian_network_tmp1.xdsl";  
		String output = "posteriors.csv";   
		String quiz = ws + "ci_input_model_order.csv";

		DataSet ds = new DataSet();
		Network net = new Network();
		net.readFile(model);
		ds.readFile(quiz);
		
		// map a column index to a network node index
		Hashtable<Integer, Integer> ref = instance.getDataMatch(net, ds);  
		String[] given_concepts = new String[]{"c_c1", "c_c2", "c_c3", "c_c4"};
		int[] question_idx = instance.getQuestionIdx(ref);
		
		// make sure the column ids are sorted in ascending order 
		Arrays.sort(question_idx);
		instance.estimatePosteior(quiz, question_idx, net, ds, ref, given_concepts, output);
	}
}
