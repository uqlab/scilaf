package scilaf.examples;

import scilaf.framework.LikelihoodValidation;
import scilaf.utils.CSVInstance;
import smile.Network;

public class TestLikelihoodValidation {
	final int NUM_OF_OPTIONS = 5;
	Network net;
	CSVInstance csv;
	LikelihoodValidation instance;
	
	public TestLikelihoodValidation(){
		net = new Network();
		csv = new CSVInstance();
		instance = new LikelihoodValidation();
	}
	
	public void testGenerateSyntheticData(String filename){
		instance.generateSyntheticData(filename);
	}
	
	public void testEstimateLikelihood1(String filename, String output){
		int num_of_students = 500;
		
		// for synthetic data
		String[][] data = csv.readCSV(filename);
		double[][] results = instance.estimateLikelihood1(num_of_students, net, data);
		
		String[] colnames = new String[data[0].length-1];
		for(int col=0; col<colnames.length; col++)
			colnames[col] = "q_q" + (col+1);
		csv.export(results, colnames, output);
	}
	
	public void testEstimateLikelihood2(String filename, String output){
		int num_of_students = 500;
		String[][] data = csv.readCSV(filename);
		
		// for synthetic data
		data = csv.readCSV(filename);
		String[][] values = instance.estimateLikelihood2(num_of_students, net, data);
		String[] colnames = new String[data[0].length-1];
		
		colnames = new String[values[0].length];
		for(int col=0; col<data[0].length-1; col++)
			colnames[col] = "q_q" + (col+1);
		
		for(int col=data[0].length-1; col<colnames.length; col++){
			colnames[col] = "answer_q_" + (col-(data[0].length-2));	
			System.out.println("col/name:" + colnames[col] + "/" + (col-data[0].length+1));
		}
		csv.export(values, colnames, output);
	}
	
	public void testRealData(String filename, String output_prefix){
		// for real data
		int num_of_students = 53;
		int num_of_questions = 28;
		
		char option;
		String[] colnames;
		String[][] results;
		String[][] data = csv.readCSV(filename);
		
		for(int qid=1; qid<num_of_questions; qid++){
			results = instance.estimateLikelihood3(num_of_students, net, data, qid);
			colnames = new String[results[0].length];
			for(int col=0; col<NUM_OF_OPTIONS; col++){
				option = (char) ('a' + col);
				colnames[col] = "option_" + option;
			}
			colnames[NUM_OF_OPTIONS] = "actual";
			colnames[NUM_OF_OPTIONS+1] = "likelihood";
			String output = output_prefix + "likelihood/predicted_likelihood_"+qid+".csv";
			csv.export(results, colnames, output);
		}
	}
	
	public static void main(String[] args) {
		String filename, output, ws, model_file;
		TestLikelihoodValidation test = new TestLikelihoodValidation();
		
		ws = "E:/workspace/tmp/";
		model_file = ws + "model1.xdsl";
		
		test.net.readFile(model_file);
		filename = ws + "random_data1.csv";
		
//		test.testGenerateSyntheticData(filename);
		
//		output = "predicted_likelihood_synthetic_1.csv";
//		test.testEstimateLikelihood1(filename, ws + output);
		
//		output = "predicted_likelihood_synthetic_2.csv";
//		test.testEstimateLikelihood2(filename, ws + output);
		
		filename = ws + "ci_input_model_order.csv";
		test.testRealData(filename, ws);
	}
}
