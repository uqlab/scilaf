package scilaf.examples;

import scilaf.framework.ModelBuilder;

public class TestModelBuilder {
	public static void main(String[] args) {
		String ws = "E:/workspace/tmp/";
		String input = ws + "concept_inventory_input1.txt";
		ModelBuilder mynet = new ModelBuilder();
		
		mynet.doRead(input);
		mynet.setProb();  
		
		String output = ws + "bayesian_network_tmp1.xdsl";
		mynet.writeFile(output);
		System.out.println("File complete, closing");
	}
}
