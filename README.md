#SciLAF Learning Assessment Framework

This is the repository for the Scientific based Learning Assessment Framework for student knowledge tracking. 
The main functionalities of this framework are to construct Bayesian student models, optimize the models, and 
generate individualized student models for student knowledge estimation, student misconception identification, 
and evaluation of questions design. 

*Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, and Gabriel Terejanu. Assessment Framework of Student 
Knowledge using Bayesian Networks. to be submitted to Education and Computers.*


#Dependencies
This software is dependent on the following packages: 

JSMILE: [https://www.bayesfusion.com/](Link URL) 

opencsv: [http://opencsv.sourceforge.net/](Link URL)


#Contents
This package contains the core classes for the learning framework in the "src" directory. It also contains several 
examples and datasets in the "examples" directory to help you better understand the framework.


#License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public 
License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any 
later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

#Acknowledgement
This material is based upon work supported by the National Science Foundation under Grant No. 1504728.