/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

import java.awt.Color;

public class Question {
	public String aws;
	public int idx;
	public String name;
	Color color;
	
	public Question(int idx){
		this.idx = idx;
		this.color = Color.blue;
	}
	
	public Question(String name){
		this.name = name;
		this.color = Color.blue;
	}
	
	public void setIdx(int idx){
		this.idx = idx;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public void setAnswer(String answer){
		this.aws = answer;
	}
	
	public void setColor(Color color){
		this.color = color;
	}
	
	public int getIdx(){
		return this.idx;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getAnswer(){
		return this.aws;
	}
	
	public Color getColor(){
		return this.color;
	}
}
