/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.io.File;
import java.io.FileReader;

import com.opencsv.CSVReader;

import scilaf.utils.*;
import smile.Network;
import smile.learning.DataMatch;
import smile.learning.DataSet;
import smile.learning.EM;

public class BayesianInference {
	final int START_IDX = 19;		
	final int END_IDX = 21;
	final int NUM_OF_SAMPLES = 20;
	final int NUM_RANDOM_QUESTION = 5;
	final int MIN_QUESTION = 10;
	final int RANDOM_RANGE = 15;
	final int NUM_QUESTIONS_EXAM2 = 4;
	final int NUM_OF_STUDENTS = 53;
	final int NUM_OF_QUESTIONS = 27;
	final int NUM_OF_CONCEPTS = 4;
	final int NUM_OF_MORE_COLS = 10; 
	final double UNKNOWN_MIN = 0.1; 
	final int ROUND_TIMES = 10000;  
	final int SAMPLE_SIZE = 82;  
	final String CONCEPT_C1 = "c_c1";
	final static double THRESHOLD = 1;  
	final static String MODEL = "model1";
	final static String QUESTION_PREFIX = "q_q";
	final String[] COL_NAMES = {"known", "unknown"};
	final double[] CONCEPTS = {0.95, 0.05, 0.05, 0.95};
	final Character[] ANSWERS = {'d', 'e', 'c', 'd', 'd', 'd', 'e', 'b', 'd', 'c', 
			'c', 'c', 'b', 'b', 'e', 'd', 'd', 'c', 'a', 'a', 'a', 'b', 'd', 'b', 
			'd', 'b', 'a'};
	final double[][] KWN= {{0.8, 0.05, 0.05, 0.05, 0.05}, {0.05, 0.8, 0.05, 0.05, 0.05}, 
			{0.05, 0.05, 0.8, 0.05, 0.05}, {0.05, 0.05, 0.05, 0.8, 0.05},
			{0.05, 0.05, 0.05, 0.05, 0.8}};   // other options: {0.9, 0.025, 0.025, 0.025, 0.025} or {0.96, 0.01, 0.01, 0.01, 0.01}
	
	Network net;
	final String[] question_list = new String[27];
	final HashMap<String, Character> map = new HashMap<>();
	
	public BayesianInference(){
		net = new Network();
		for(int i=1; i<28; i++)
			question_list[i-1] = "q_q"+i;
		for(int i=0; i<27; i++)
			map.put(question_list[i], ANSWERS[i]);
	}
	
	/* Optimize concepts with log evidence
	 * @param concept_target the target concept
	 * @param questions_all all the question nodes
	 * @param fixed_nodes_concepts fixed concept nodes
	 * @param fixed_nodes_questions fixed question nodes
	 * @param ds_all the dataset with all the quizzes
	 * @param network_prior_opm  name of the output network file
	 */
	public void optimizeConceptLogEvidence(
		String concept_target, 
		String[] questions_all, 
		String[] fixed_nodes_concepts, 
		String[] fixed_nodes_questions, 
		String ds_all, 
		String network_prior_opm){
		
		double current_log_em = 0;
		double last_log_em = Double.MAX_VALUE;
		DataSet ds = new DataSet();
		ds.readFile(ds_all);
		DataMatch[] matching = ds.matchNetwork(net);
		while(Math.abs(current_log_em - last_log_em) > THRESHOLD){
			last_log_em = current_log_em;
			// fix all concepts nodes except for the questions nodes
			optimize(ds, matching, fixed_nodes_concepts, questions_all); 
			// fix all questions nodes except for the concepts nodes
			current_log_em = optimizeConcept(ds, matching, fixed_nodes_questions, 
						fixed_nodes_concepts); 
			System.err.println("Current log_em is:" + current_log_em);
		}
		System.err.println("final optimized prior with all data:" + 
				net.getNodeDefinition(concept_target)[0] + " -> " + Math.abs(current_log_em - last_log_em));

		// update CPT for knowing concepts after the optimization of the prior
		for(String question: questions_all) 
/*			resetKnownCPT(question);*/
			adjustKnownCPT(question);
		for(String concept: fixed_nodes_concepts)
			resetConceptCPT(concept);
				
		// export first optimized network with quiz 1 data
		net.writeFile(network_prior_opm + ".xdsl");
		System.out.println("finish generating an optimized model ~~~~");
	}
		
	/* Optimize the prior given all quiz data */
	public void optimizePriorLogEvidence(
		String concept_target, 
		String[] questions_all, 
		String[] fixed_nodes_concept, 
		String[] fixed_nodes_questions, 
		String ds_all, 
		String network_prior_opm){
		
		double last_log_em = Double.MAX_VALUE;
		double current_log_em = 0;
		DataSet ds = new DataSet();
		ds.readFile(ds_all);
		DataMatch[] matching = ds.matchNetwork(net);
		while(Math.abs(current_log_em - last_log_em) > THRESHOLD){
			last_log_em = current_log_em;
			// fix all the concepts except for the questions
			optimize(ds, matching, fixed_nodes_concept, questions_all); 
			// fix all nodes except for the concept
			current_log_em = optimize(ds, matching, fixed_nodes_questions); 
			System.err.println("Current log_em is:" + current_log_em);
		}
		System.err.println("final optimized prior with all data:" 
				+ net.getNodeDefinition(concept_target)[0] + " -> " 
				+ Math.abs(current_log_em - last_log_em));
		
		// update CPT for knowing concepts after the optimization of the prior
		for(String question:questions_all) 
/*			adjustKnownCPT(question);*/
			resetKnownCPT(question);		

		// get the conditionals, such as P(Option=a|Concept=known), from the network
		exportCPT(questions_all);	
		
		// export first optimized network with quiz 1 data
		net.writeFile(network_prior_opm + ".xdsl");
		System.out.println("finish generating an optimized model ~~~~");
	}
		
	/* Optimize the prior given all quiz data */
	public Network optimizePrior(
		String concept_target, 
		String[] questions_all, 
		String[] fixed_nodes_concept, 
		String[] fixed_nodes_questions, 
		Network anet, 
		String ds_all, 
		String network_prior_opt){
		
		double prior, new_prior;
		double delta_prior = Double.MAX_VALUE;
		DataSet ds = new DataSet();
		ds.readFile(ds_all);
		DataMatch[] matching = ds.matchNetwork(anet);
		
		prior = anet.getNodeDefinition(concept_target)[0];  // initial prior (e.g. 0.9)
		System.out.println("initial prior for concept is:" + concept_target + " -> " + prior);
		while(Math.abs(delta_prior) > THRESHOLD){
			optimize(ds, matching, fixed_nodes_concept, questions_all);
			new_prior = optimize(ds, matching, fixed_nodes_questions);
			System.err.println("Current prior is:" + new_prior);
			delta_prior = new_prior - prior;
			prior = new_prior;
		}
		System.err.println("final optimized prior with all data:" + prior + " -> " + delta_prior);
		
		// update CPT for knowing concepts after the optimization of the prior
		for(String question:questions_all) 
			adjustKnownCPT(question);
		
		// export first optimized network with quiz 1 data
		anet.writeFile(network_prior_opt + ".xdsl");
		System.out.println("finish generating an optimized model ~~~~");
		return anet;
	}
		
	/* A stepwise procedure for rest quizzes
	 * @param questions_quiz questions for a given quiz
	 * @param fixed_nodes fixed nodes
	 * @param ds_quiz data for a fiven quiz
	 * @param network_by_quiz name of the output network file
	 * @return an optimized Bayesian network
	 */
	public Network stepwiseOpmLogEvidence(
		String[] questions_quiz, 
		String[] fixed_nodes, 
		String ds_quiz, 
		String network_by_quiz){
		
		// make sure that only questions from a given quiz match the network
		DataMatch[] matching = new DataMatch[questions_quiz.length]; 
		DataSet ds = new DataSet();
		ds.readFile(ds_quiz);
		matching = ds.matchNetwork(net);
		double last_log_em = Double.MAX_VALUE;
		double current_log_em = 0;
		while(Math.abs(current_log_em - last_log_em) > THRESHOLD){
			last_log_em = current_log_em;
			current_log_em = runEM(ds, matching, fixed_nodes);
			System.err.println("Current log_em is:" + current_log_em);
		}
		System.err.println("final em with all data:" + current_log_em + " -> " 
				+ Math.abs(current_log_em - last_log_em));
		
		// update CPT for knowing concepts after the optimization of the prior
		for(String question:questions_quiz)
			adjustKnownCPT(question);
		for(String question: questions_quiz)
			System.out.println("after this quiz, unknown conditionals for " 
					+ question + " :" + net.getNodeDefinition(question)[4]);
		net.writeFile(network_by_quiz + ".xdsl");
		return net;
	}
	
	/* A stepwise procedure for rest quizzes */
	public void stepwiseOpm4Rest(
		String[] questions_quiz, 
		String[] fixed_nodes, 
		String ds_quiz, 
		String network_by_quiz){
		
		// make sure only questions_quiz2 match, remove other quizzes questions
		DataMatch[] matching = new DataMatch[questions_quiz.length];  
		DataSet ds = new DataSet();
		ds.readFile(ds_quiz);
		matching = ds.matchNetwork(net);
		runEM(ds, matching, fixed_nodes);
		for(String question:questions_quiz)
			adjustKnownCPT(question);
		for(String question: questions_quiz)
			System.out.println("after this quiz, unknown conditionals for " + question 
					+ " :" + net.getNodeDefinition(question)[4] + "," 
					+ net.getNodeDefinition(question)[5] + "," + net.getNodeDefinition(question)[6]
					+ "," + net.getNodeDefinition(question)[7]);
		net.writeFile(network_by_quiz + ".xdsl");
	}
	
	/* Optimize quiz1 with log evidence */
	public void optimizeQuiz1LogEvidence(
		String concept_target, 
		String[] questions_all, 
		String[] fixed_nodes_concept, 
		String[] fixed_nodes_questions, 
		Network anet, 
		String ds_quiz1, 
		String network_quiz1){
		
		double last_log_em = Double.MAX_VALUE;
		double current_log_em = 0;
		DataSet ds = new DataSet();
		ds.readFile(ds_quiz1);
		DataMatch[] matching = ds.matchNetwork(anet);
		while(Math.abs(current_log_em - last_log_em) > THRESHOLD){
			last_log_em = current_log_em;
			optimize(ds, matching, fixed_nodes_concept, questions_all);
			current_log_em = optimize(ds, matching, fixed_nodes_questions);
			System.err.println("Current log_em is:" + current_log_em);
		}
		System.err.println("final optimized prior with all data:" 
				+ anet.getNodeDefinition(concept_target)[0] + " -> " 
				+ Math.abs(current_log_em - last_log_em));
		
		// update CPT for knowing concepts after the optimization of the prior
		for(String question:questions_all) 
			adjustKnownCPT(question);
		
		// export first optimized network with quiz 1 data
		anet.writeFile(network_quiz1 + ".xdsl");
		System.out.println("finish generating an optimized model ~~~~");
	}
		
	/* Optimize the prior with a given threshold */
	public void optimizeQuiz1(
		String concept_target, 
		String[] questions_quiz1, 
		String[] fixed_nodes_concept, 
		String[] fixed_nodes_questions, 
		Network anet, 
		String ds_quiz1, 
		String network_quiz1){
		
		double prior, new_prior;
		double delta_prior = Double.MAX_VALUE;
		DataSet ds = new DataSet();
		ds.readFile(ds_quiz1);
		DataMatch[] matching = ds.matchNetwork(anet);
		
		// initial prior (e.g. 0.9). Also, concepts_quiz1 only includes one concept here.
		prior = anet.getNodeDefinition(concept_target)[0];  
		System.out.println("initial prior for concept is:" + concept_target + " -> " + prior);
		while(Math.abs(delta_prior) > THRESHOLD){
			optimize(ds, matching, fixed_nodes_concept, questions_quiz1);
			new_prior = optimize(ds, matching, fixed_nodes_questions);
			System.err.println("Current prior is:" + new_prior);
			delta_prior = new_prior - prior;
			prior = new_prior;
		}
		System.err.println("final optimized prior with quiz1 data:" + prior + " -> " + delta_prior);
		
		// update CPT for knowing concepts after the optimization of the prior
		for(String question:questions_quiz1) 
			adjustKnownCPT(question);
		
		// export first optimized network with quiz 1 data
		anet.writeFile(network_quiz1 + ".xdsl");
		System.out.println("finish generating model 1 ~~~~");
	}
	
	/* Execute the optimization procedure for each question,
     * can switch between adjust/reset mode
     * @param ds a dataset with answers
     * @param matches data matches
     * @param nodes selected nodes for the EM algorithm
     * @param questions an array of question names
     */
	public void optimize(DataSet ds, DataMatch[] matches, String[] nodes, String[] questions){
		runEM(ds, matches, nodes);
		for(String question: questions)
/*			adjustKnownCPT(question);*/
			resetKnownCPT(question);
	}
		
	/* Execute the optimization for the given concepts */
	public double optimize(DataSet ds, DataMatch[] matches, String[] nodes){
		double log_em = 0;
		log_em = runEM(ds, matches, nodes);
		return log_em;
	}
	
	/* Execute the optimization for given concepts */
	public double optimizeConcept(DataSet ds, DataMatch[] matches, String[] nodes, String[] concepts){
		double log_em = 0;
		log_em = runEM(ds, matches, nodes);
		for(String concept: concepts)
			resetConceptCPT(concept);
		return log_em;
	}
	
	/* Execute the optimization for the base concept */
	public double optimizePrior(DataSet ds, DataMatch[] matches, String[] nodes, String concept){
		double prior = 0;
		runEM(ds, matches, nodes);
		prior = net.getNodeDefinition(concept)[0];
		return prior;
	}
		
	/* Execute the EM procedure */
	public double runEM(DataSet ds, DataMatch[] matches, String[] nodes){
		// EM settings (no randomized initial parameters, set confidence, uniformize, enable relevance)
		EM em = new EM();
		em.setRandomizeParameters(false);
		
		// confidence level, low means even small amount of data can change the local pdf a lot
		em.setEqSampleSize(1);   
		
		// only necessary to enable relevance when there is propagated evidence
		em.setRelevance(false);  
		em.setUniformizeParameters(false);
		em.learn(ds, net, matches, nodes);
		net.updateBeliefs();
		return em.getLastScore();
	}
	
	/* Reset conditional probability tables of concepts to initial settings */
	public void resetConceptCPT(String concept){
		double[] defs;
		defs = net.getNodeDefinition(concept); 
		if(defs.length>2){
			defs[0]=CONCEPTS[0];  // e.g. reset it to 0.95
			defs[1]=CONCEPTS[1];  // e.g. reset it to 0.05
			defs[defs.length-2]=CONCEPTS[2];  // e.g. reset it to 0.05
			defs[defs.length-1]=CONCEPTS[3];  // e.g. reset it to 0.95
		}
		net.setNodeDefinition(concept, defs);  
		net.updateBeliefs();
	}
		
	/* Adjust the conditional probability tables for a given question */
	public void adjustKnownCPT(String question){
		int idx = 0;
		double[] defs;
		double diff;
		int size = KWN[0].length;
		double[] unknown = new double[size];
		defs = net.getNodeDefinition(question);

		for(int j=0; j<size; j++){
			// get the question idx
			idx = map.get(question)-'a';  
			defs[j] = KWN[idx][j];
		}			

		/* adjust the unknown probabilities */
		for(int k=0; k<(defs.length-size)/size; k++){
			unknown = new double[size];
			for(int j=0; j<size; j++)
				unknown[j]=defs[size+k*size+j]; 
			Arrays.sort(unknown);  
			while(unknown[0]<UNKNOWN_MIN){
				diff = UNKNOWN_MIN-unknown[0];
				unknown[0] = UNKNOWN_MIN;
				unknown[size-1]-=diff;
				
				// unknown[0] is the lowest while unknown[size-1] is the highest
				Arrays.sort(unknown);		
			}
			for(int j=0; j<size; j++)
				// update defs values with adjusted values
				defs[size+k*size+j] = unknown[j]; 
		}
		net.setNodeDefinition(question, defs);
		net.updateBeliefs();
	}
		
	/* Reset conditional probability tables of questions to initial settings */
	public void resetKnownCPT(String question){
		int idx = 0;
		double[] defs;
		defs = net.getNodeDefinition(question); 

		for(int j=0; j<KWN[0].length; j++){
			// get the question idx
			idx = map.get(question)-'a';  
			defs[j] = KWN[idx][j];
		}			
		net.setNodeDefinition(question, defs);  
		net.updateBeliefs();
	}
	
	/* Get the conditional probability tables' names */
	public String[] getTableNames(String question){
		int psize;   // number of parents nodes
		int csize;	// number of columns
		String[] pnames;	// names of parents nodes
		String[] names;		// column name
		pnames = net.getParentIds(question); //String[] names
		psize=pnames.length;
		csize=COL_NAMES.length;
		
		// initialize table column names
		names=new String[pnames.length*2+1]; // "option"+concept.known+concept.unknown 
		names[0] = "option";
		for(int i=1;i<names.length;i++){
			names[i]=pnames[(i-1)/names.length]+"_"+COL_NAMES[(i-1)%csize];
			for(int j=1;j<psize;j++){			
				names[i]+="_"+pnames[j]+"_"+COL_NAMES[(i-1)%csize];
				System.out.println("name[i]:"+names[i]);
			}
		}
		return names;
	}
	
	/* Get the conditional probability tables' values */
	public double[][] getCPT(String question){
		double[] defs=net.getNodeDefinition(question);
		int rows=KWN[0].length;  // e.g. 5 rows
		int cols=defs.length/rows;	// e.g. 2 cols
		double[][] res=new double[rows][cols];
		
		for(int i=0;i<defs.length;i++){
			res[i%rows][i/rows] = defs[i];
		}
		return res;
	}
	
	/* Export conditional probability tables for given questions 
	 * @param questions names of question nodes to produce conditional probability tables
	 */
	public void exportCPT(String[] questions){
		String filename = "";
		String[] colnames;
		String[][] str_values;
		double[][] values;
		CSVInstance csv=new CSVInstance();
		
		for(String question:questions){
			filename = "cpt_"+question+".csv";
			values = getCPT(question);
			str_values = new String[values.length][values[0].length+1];
			for(int i=0;i<values.length;i++){
				str_values[i][0] = String.valueOf((char) ('a'+i));
				for(int j=0;j<values[i].length;j++){
					// only keep ROUND_TIMES digits
					values[i][j]= (double) Math.round(values[i][j]*ROUND_TIMES)/ROUND_TIMES;  
					
					// convert digits to Strings, and also add an "option" column
					str_values[i][j+1] = "" + values[i][j];	
				}
			}
			colnames = getTableNames(question);
			csv.export(str_values, colnames, filename);
		}
		System.out.println("finish exporting conditional table" + filename);
	}

	/* Get a set of random question samples */
	public Set<String> getRandomSamples(){
		int i=0;
		int num;
		String qname = "";
		Random rdm = new Random();
		Set<String> samples = new HashSet<>();
		while(i < NUM_RANDOM_QUESTION){
			// get a random question id
			num = rdm.nextInt(RANDOM_RANGE)+MIN_QUESTION;  
			qname = "q_q"+num;
			if(!samples.contains(qname)){
				samples.add(qname);
				i++;
				System.out.println("current random num is:" + qname);
			}
		}
		return samples;
	}
	
	/* Get the question index */
	public int[] getQuestionIdx(Hashtable<Integer, Integer> ref){
		int[] idx = new int[ref.keySet().size()];
		int i=0;
		for(int key: ref.keySet())
			idx[i++] = key;
		return idx;
	}

	/* Get the concepts names: the hashtable ref need to include all questions nodes,
	 * thus the table is usually obtained using the data of all quizzes
	 * @param ref a hashtable maps a column index to a network node index
	 * @param net a Bayesian network
	 * @return an array of names of concept nodes
	 */	
	public String[] getConcepts(Hashtable<Integer, Integer> ref, Network net){
		String[] concepts;
		List<String> concepts_nodes = new ArrayList<String>();
		int[] all_nodes_idx = net.getAllNodes();
		
		ArrayList<Integer> question_nodes_idx = new ArrayList<Integer>(ref.values());
		for(int idx: all_nodes_idx){
			if(!question_nodes_idx.contains(idx))
				concepts_nodes.add(net.getNodeId(idx));
		}	
		concepts = new String[concepts_nodes.size()];
		for(int i=0; i<concepts.length; i++)
			concepts[i] = concepts_nodes.get(i);
		return concepts;
	}

	/* Map each column index to the corresponding node id */
	public Hashtable<Integer, Integer> getDataMatch(Network net, DataSet ds){
		DataMatch[] dms;
		Hashtable<Integer, Integer> question2node = new Hashtable<Integer, Integer>();
		dms = ds.matchNetwork(net);
		for(DataMatch dm:dms)
			question2node.put(dm.column, dm.node);
		return question2node;
	}

	/* Create individualized student models */
	public void exportIndividualizedStudentModel(String csvfile, String updated_name, String model){
		double posterior;
		String updated_model_name;
		String[][] data; 
		double[] defs;
		CSVInstance csv = new CSVInstance();
		data = csv.readCSV(csvfile);
		net.readFile(model);
		defs = net.getNodeDefinition(CONCEPT_C1);
		
		/*read each posterior for each student, set it as prior, udpate the model*/ 
		for(int i=0;i<data.length;i++){
			updated_model_name = updated_name + (i+1) + ".xdsl";
			posterior = Double.valueOf(data[i][1]);
			defs[0] = posterior;   // update prior for each student
			defs[1]= 1-posterior;
			net.setNodeDefinition(CONCEPT_C1, defs);
			net.updateBeliefs();
			net.writeFile(updated_model_name);  // export each individualized student model
		}
	}

	/* Estimate the posteriors
	 * @param csvfile a csv file with student information
	 * @param question_idx an array of question indexes
	 * @param net a Bayesian network
	 * @param ds a dataset with quiz data
	 * @param ref a hashtable with question ids and the related answers
	 * @param concept_names an array of concepts' names
	 * @param filename an output filename for the posteriors
	 */
	public void estimatePosteior(
		String csvfile, 
		int[] question_idx, 
		Network net, 
		DataSet ds, 
		Hashtable<Integer, Integer> ref, 
		String[] concept_names, 
		String filename){
		
		String[] line;
	    CSVReader reader = null;
	    List<Student> students = new ArrayList<Student>();
	    try {
	        reader = new CSVReader(new FileReader(csvfile), ',', '"', 1);
	        while ((line = reader.readNext()) != null) {
	        	Student student = new Student(line[0]);
		    	
		    	/*initialize each Student class*/
		    	student = initStudent(student, question_idx, concept_names, ds, line);
		    	
				/*update posteriors for each student class*/
				student = updateConceptByStudent(student, line, question_idx, net, ref);
				students.add(student);
	        }
	        
	        /*export student data*/
	        CSVInstance acsv = new CSVInstance();
	    	acsv.export(students, ds, question_idx, concept_names, filename);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	/* Estimate posteriors with individualized models: the first column of the 
	 * input file is the student id */
	public void estimatePosteriorWithIndividualizedModel(
		String path, 
		String input_file, 
		String output_file, 
		DataSet ds, 
		int[] question_idx, 
		String[] concept_names, 
		Hashtable<Integer, Integer> ref){
		
		CSVInstance csv;
		String[] fnames;
		String[][] data;
		List<Student> students = new ArrayList<>();
		Network[] nets = new Network[NUM_OF_STUDENTS];
		csv = new CSVInstance();
		data = csv.readCSV(input_file);
		
		/*read filenames from a folder*/
		fnames = getFileNames(path);
		
		 /*initialize each individualized student network*/
		for(int i=0;i<fnames.length;i++){
			nets[i] = new Network();
			nets[i].readFile(fnames[i]);
		}
		
		for(int sid=0;sid<data.length;sid++){
			Student student = new Student(data[sid][0]);  // student id
	    		    	
	    	/*initialize each Student class*/
	    	student = initStudent(student, question_idx, concept_names, ds, data[sid]);
	    	
			/*update posteriors for each student class*/
			student = updateConceptByStudent(student, data[sid], question_idx, nets[sid], ref);
			students.add(student);
		}
		
		/*export student data*/
    	csv.export(students, ds, question_idx, concept_names, output_file);
    	System.out.println("finishi exporting posteriors file");
	}
	
	/* Estimate the posteriors with random samples */
	public void estimatePosteriorWithRandomSampling(
		String csvfile, 
		int[] question_idx, 
		Network net, 
		DataSet ds, 
		Hashtable<Integer, Integer> ref, 
		String[] concept_names, 
		String filename){
		
		String[] line;
    	int cid = 0;
    	int rid = 0;
		int correct_cnt = 0;
		int rdm_cnt = 0;
    	String qname = "";
    	String random_string = "";
		CSVReader reader = null;
		Set<String> rdm_cols = new HashSet<>();
		Set<String>[] samples = new HashSet[NUM_OF_SAMPLES];
		Set<String> sample = new HashSet<>();
		// +1 column for student name
		String[][] data = new String[NUM_OF_STUDENTS][NUM_OF_QUESTIONS+1];  
		// contains questions related to concept 3; check
		String[][] result = new String[NUM_OF_STUDENTS*NUM_OF_SAMPLES][data[0].length+NUM_OF_MORE_COLS];   
		String[] colnames = new String[result[0].length];
		Iterator<String> iterator;
		
		try {
			reader = new CSVReader(new FileReader(csvfile), ',', '"', 1);
			// change 82 to a static variable if necessary
			while((line = reader.readNext()) != null && rid < SAMPLE_SIZE) {  
				for(int i=0; i<line.length; i++)
					data[rid][i] = line[i];
				rid++;
			}
		} catch (Exception ee){
			ee.printStackTrace();
		}
		
		/* get a random sample at one time, and store it into an array */
		for(int iteration=0; iteration<NUM_OF_SAMPLES; iteration++){
			sample = getRandomSamples();
			samples[iteration] = sample;
		}
		
		for(int id=0; id<data.length; id++){
			Student student = new Student();
	    	
	    	/*initialize each Student class*/
	    	student = initStudent(student, question_idx, concept_names, ds, data[id]);
			
			for(Concept concept: student.getConcepts()){
				concept.setPrior(net.getNodeValue(concept.getName())[0]);
				System.out.println("set the prior for " + concept.getName() + ":" + concept.getPrior());
			}
        	
        	for(int iteration=0; iteration<NUM_OF_SAMPLES; iteration++){
        		net.clearAllEvidence();
    			net.updateBeliefs();
        		student.setUserName(iteration + "_" + data[id][0]);  // a student name is in format 1_S1
				rdm_cols = samples[iteration];
				
				for(int idx: question_idx){
					qname = QUESTION_PREFIX + (idx+9);	
					// remove questions selected by random sampling
	        		if(rdm_cols.contains(qname))   
	        			continue;
	        		if(data[id][idx].trim().length()!=0 && !data[id][idx].trim().equals("null"))
	        			// set evidence for the node with the related answer
	        			net.setEvidence(ref.get(idx), data[id][idx].toLowerCase());   
	        	}
				// update the network to get posterior
				net.updateBeliefs();  
	        	for(Concept concept: student.getConcepts()){
	        		// set the posterior after inserting the evidence to the network
	        		concept.setPosterior(net.getNodeValue(concept.getName())[0]);  
	        		System.out.println("set the posterior for " + concept.getName() 
	        			+ " :" + concept.getPosterior());
	        	}
	        	
	        	/*store information into a result array*/
	        	// same student for different sampling iteration
	        	rid = iteration*data.length + id;		
	        	result[rid][0] = student.username;
	        	for(int i=0; i<question_idx.length; i++){
	        		qname = "q_q" + (question_idx[i]+9);
	        		if(rdm_cols.contains(qname))
	        			continue;
	        		result[rid][i+1] = student.questions[i].aws;
	        	}
	        	for(int i=0; i<student.getConcepts().length; i++){
	        		result[rid][2*i+1+question_idx.length] = "" + student.getConcepts()[i].prior;
	        		// each concept has a prior value and a posterior value
	        		result[rid][2*i+1+1+question_idx.length] = "" 
	        				+ student.getConcepts()[i].posterior;  
	        	}
	        	
	        	/*count the questions with correct answers*/
	        	correct_cnt = 0;
	        	iterator = rdm_cols.iterator();
	        	rdm_cnt = 0;
	        	random_string = "";
	        	while(iterator.hasNext()){
	        		qname = iterator.next();
	        		// convert question name to question idx	
	        		cid = Integer.valueOf(qname.substring(3, qname.length()))-9;  
	        		System.out.println("cid is:" + qname + "/" + cid);
	        		random_string += qname;
	        		if(rdm_cnt<4)
	        			random_string += ";";
	        		if(String.valueOf(map.get(qname)).equals(data[id][cid]))
	        			correct_cnt++;
	        		rdm_cnt++;
	        	}
	        	// result[rid][result[0].length-2]
	        	cid = 1+question_idx.length+2*student.getConcepts().length;
	        	result[rid][cid] = "" + correct_cnt;  
	        	result[rid][result[0].length-1] = random_string;
			}
        	colnames[0] = "sid";
        	
    		/*append questions names to columns names*/
        	for(int i=0; i < question_idx.length; i++)
    			colnames[i+1] = ds.getVariableId(question_idx[i]);
    		for(int i=0; i<concept_names.length; i++){	
    			colnames[question_idx.length+1+2*i+0] = concept_names[i]+"_prior";
    			colnames[question_idx.length+1+2*i+1] = concept_names[i]+"_posterior";
    		}
    		colnames[result[0].length-2] = "correct count";
    		colnames[result[0].length-1] = "random questions";
		}
		CSVInstance acsv = new CSVInstance();
		acsv.export(result, colnames, filename);
	}	
	
	/* Get a list of file names from a folder */
	private String[] getFileNames(String path){
		int idx;
		String digit_str;
		String fname;
		File[] files;
		String[] filenames = new String[NUM_OF_STUDENTS];
		
		// read filenames from folder
		files = new File(path).listFiles();
		if(files.length==0)
			System.out.println("Please check your path!");
		for(File file: files){
			if(file.isFile()){
				fname = file.getName();
				// get the digits from a filename
				digit_str = fname.substring(START_IDX, END_IDX);  
				idx = 0;	
				for(char ch: digit_str.toCharArray()){
					if(ch>='0' && ch<='9'){
						idx = idx*10 + (ch-'0');
					}
				}
				filenames[idx-1] = path + fname;
			}	
		}		
		for(String name: filenames)
			System.out.println("file name is:" + name);
		return filenames;
	}

	/* Initialize a Student instance */
	private Student initStudent(
		Student student, 
		int[] question_idx, 
		String[] concept_names, 
		DataSet ds, 
		String[] line){
		
		/*set concepts for each student*/
		student.setConcepts(concept_names);
		Question[] questions = new Question[question_idx.length];
		for(int qid=0; qid < question_idx.length; qid++){
			questions[qid] = new Question(question_idx[qid]);
			questions[qid].setName(ds.getVariableId(question_idx[qid])); 
			if(ds.getVariableId(question_idx[qid])!=null)  
				questions[qid].setAnswer(line[question_idx[qid]]);  
			System.out.println("answer for question is:" + questions[qid].name + "/" 
					+ questions[qid].aws);
		}
		
		/*set questions for each student*/
		student.setQuestions(questions);
		return student;
	}

	/* Update the priors and posteriors for each concept given a student */
	private Student updateConceptByStudent(
		Student student, 
		String[] line, 
		int[] question_idx, 
		Network net, 
		Hashtable<Integer, Integer> ref){
		Concept[] concepts = student.getConcepts();
		
		/*set priors*/
		net.clearAllEvidence();
		net.updateBeliefs();
		for(Concept concept: concepts){
			// set an prior as the probability for the known situation
	    	concept.setPrior(net.getNodeValue(concept.getName())[0]);  
			System.out.println("set the prior for " + concept.getName() 
					+ " :" + concept.getPrior());
		}
		/*update posteriors*/
		for (int idx: question_idx){
			if(line[idx].trim().length()!=0 && !line[idx].trim().equals("null"))
				// set evidence for the node with the related answer
				net.setEvidence(ref.get(idx), line[idx].toLowerCase());   
		}
		// update the network to get posteriors
		net.updateBeliefs();  
		for(Concept concept: concepts){
			// update the posterior with new inserted evidence
			concept.setPosterior(net.getNodeValue(concept.getName())[0]);  
			System.out.println("set the posterior for " + concept.getName() 
					+ " :" + concept.getPosterior());
		}
		student.updateConcepts(concepts);
		return student;
	}
}
