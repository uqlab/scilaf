/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import smile.Network;
import smile.SMILEException;

public class ModelBuilder {
	final int ANSWER_SIZE = 5;
	final double[] PRIOR = {0.7, 0.3};
	final double[] UNKNOWN = {0.2, 0.2, 0.2, 0.2, 0.2};
	final double[][] KNOWN= {{0.8, 0.05, 0.05, 0.05, 0.05}, {0.05, 0.8, 0.05, 
			0.05, 0.05}, {0.05, 0.05, 0.8, 0.05, 0.05}, {0.05, 0.05, 0.05, 
			0.8, 0.05}, {0.05, 0.05, 0.05, 0.05, 0.8}};
	final Character[] ANSWERS = {'d', 'e', 'c', 'd', 'd', 'd', 'e', 'b', 'd',
			'c', 'c', 'c', 'b', 'b', 'e', 'd', 'd', 'c', 'a', 'a', 'a', 'b', 
			'd', 'b', 'd', 'b', 'a'};
	Network net;
	final HashMap<Integer, Character> map;
	
	public ModelBuilder(){
		net = new Network();
		map = new HashMap<>(27);
		for(int i=0; i<27; i++)
			map.put(i, ANSWERS[i]);
	}
		
	/* Create a concept node */
	public  void initConcept(String coname){ 
		System.out.println(coname + " initialized as a concept node");
		net.addNode(Network.NodeType.Cpt, coname);
		net.addOutcome(coname, "known");
		net.addOutcome(coname, "unknown");
		net.deleteOutcome(coname, 0);
	    net.deleteOutcome(coname, 0);
	    net.setNodeBgColor(coname, Color.red);
	}
	
	/* Create a question node */
	public  void initQuestion(String quname){
		char option;
		System.out.println(quname + " initialized as a question node");
		net.addNode(Network.NodeType.Cpt, quname);
		for(int i=0; i<ANSWER_SIZE; i++){
			option = (char) ('a' + i);
			net.addOutcome(quname, "" + option);
		}	
		net.deleteOutcome(quname, 0);
	    net.deleteOutcome(quname, 0);
	    net.setNodeBgColor(quname, Color.LIGHT_GRAY);
	}
	
	/* Connect concept nodes */
	public void linkConcept(String[] tempArray){ 
		System.out.println("Concept link established");
		for(int i=1; i<tempArray.length; i++){
			net.addArc(tempArray[0].trim(), tempArray[i].trim());
			System.out.println("Linking /" + tempArray[0] + "/ to /" 
					+ tempArray[i] + "/");
		}
	}
	
	/* Connect question nodes */
	public void linkQuestion(String[] tempArray){
		System.out.println("Question link established");
		for(int i=1; i<tempArray.length; i++){
			System.out.println("Linking " + tempArray[0].trim() + " to " 
			+ tempArray[i].trim());
			net.addArc(tempArray[0].trim(), tempArray[i].trim());
		}
	}
	
	/* Generate a network from a text file */
	public void doRead(String textfile){
		try{
			String buf = null;
			String bufcheck = null;
			boolean linkline = false;
			String[] tempLink = null;
			BufferedReader br = new BufferedReader(new FileReader(textfile));
			
			while ((buf = br.readLine()) != null) {
				if(buf.trim().length() > 2){
		            bufcheck = buf.trim().substring(0,2);
		            
		            //check to see if it's a linking line
		            if(buf.contains(",")) {linkline = true;} 
		            
		            //check for concepts
		            if(bufcheck.toLowerCase().equals("c_")){
		            	if(linkline){
		            		tempLink = buf.split(",");
		            		linkConcept(tempLink);
		            	} else {
		            		initConcept(buf);
			            	System.out.println(buf + ": is a concept line");
		            	}
		            //check for questions
		            } else if (bufcheck.toLowerCase().equals("q_")){
		            	if(linkline){
		            		tempLink = buf.split(",");
		            		linkQuestion(tempLink);
		            	} else {
		            		System.out.println(buf + ": is a question line");
		            		initQuestion(buf);
		            	}
		            } else {
		            	//check for mistypes
		            	System.err.println("LINE IS IMPROPERLY FORMATTED:" + buf);
		            }
				}
				//else, the line is either blank or too short; skipped
	            linkline = false;
			}
			br.close();
		} catch(Exception ee) {
			ee.printStackTrace();
		}
		finally{ 
			System.out.println("Read process: Complete");
		}
	}
	
	/* Set probabilities for a simple concept node */
	public void setSimpleConceptProb(String node){
		net.setNodeDefinition(node, PRIOR);
		net.updateBeliefs();
	}
	
	/* Set probabilities for a simple question node */
	public void setSimpleQuestionProb(String node, char answer, int size){
		double[] defs = new double[size];
		int anw_idx = answer - 'a';  // get the index of the answer to get the known value
		for(int i=0; i<ANSWER_SIZE; i++)
			defs[i] = KNOWN[anw_idx][i];
		for(int i=ANSWER_SIZE; i<defs.length; i++)
			defs[i] = UNKNOWN[i-ANSWER_SIZE];
		net.setNodeDefinition(node, defs);
	}
	
	/* Set probabilities for complex concept nodes
	 * e.g. {0.95, 0.05, 0.05, 0.95, 0.05, 0.95, 0.05, 0.95} */
	public void setComplexConceptProb_(String node, int size){
		double[] defs = new double[size];	
		for(int i=0; i<size; i++){
			if(i==0 || (i!=1 && i%2==1))
				defs[i] = 0.95;
			else
				defs[i] = 0.05;
		}
		net.setNodeDefinition(node, defs);
		net.updateBeliefs();
	}
	
	/* Set probabilities for complex concept nodes
	 * e.g. {0.95, 0.05, 0.5, 0.5, 0.5, 0.5, 0.05, 0.95}*/
	public void setComplexConceptProb(String node, int size){
		double[] defs = new double[size];	
		for(int i=0; i<size; i++){
			if(i==0 || i==size-1)
				defs[i] = 0.95;
			else if(i==1 || i==size-2)
				defs[i] = 0.05;
			else
				defs[i] = 0.5;
		}
		net.setNodeDefinition(node, defs);
		net.updateBeliefs();
	}
	
	/* Set probabilities for complex question nodes */
	public void setComplexQuestionProb(String node, char answer, int size){
		double[] defs = new double[size];
		int anw_idx = answer - 'a';
		for(int i=0; i<ANSWER_SIZE; i++)
			defs[i] = KNOWN[anw_idx][i];
		for(int i=ANSWER_SIZE; i<size; i++)
			defs[i] = 0.2;
		net.setNodeDefinition(node, defs);
		net.updateBeliefs();
	}
	
	public void setProb(){
		double[] defs;
		String simpleConcept, complexConcept, simpleQuestion, complexQuestion;
		String[] nodes;
		int size, id;
		nodes = net.getAllNodeIds();
		char answer;
		
		for(String node: nodes){
			defs = net.getNodeDefinition(node);
			size = defs.length;
			if(size == 2){
				simpleConcept = node;
				setSimpleConceptProb(simpleConcept);
			} else if(size == ANSWER_SIZE*2) {
				simpleQuestion = node;
				id = Integer.valueOf(simpleQuestion.substring(3));
				answer = map.get(id-1);
				setSimpleQuestionProb(simpleQuestion, answer, size);
			} else if(size % ANSWER_SIZE==0){
				complexQuestion = node;
				id = Integer.valueOf(complexQuestion.substring(3));
				answer = map.get(id-1);	
				setComplexQuestionProb(complexQuestion, answer, size);
			} else{
				complexConcept = node;
				setComplexConceptProb(complexConcept, size);
			}
		}
	}
	
	public void writeFile(String output){
		this.net.writeFile(output);
	}
	
	/* Build a sample network */
	public void build_network(){		
		try {
			Network net = new Network();
			net.addNode(Network.NodeType.Cpt, "Vectors");
			net.addOutcome("Vectors", "known");
			net.addOutcome("Vectors", "unknown");
			net.deleteOutcome("Vectors", 0);
		    net.deleteOutcome("Vectors", 0);//
						
			net.addNode(Network.NodeType.Cpt, "Q1_V");
			net.addOutcome("Q1_V", "a");
			net.addOutcome("Q1_V", "b");
			net.addOutcome("Q1_V", "c");
			net.addOutcome("Q1_V", "d");
			net.deleteOutcome("Q1_V", 0);
		    net.deleteOutcome("Q1_V", 0);
			
			net.addArc("Vectors", "Q1_V");
			double[] aVectorDef = {0.95, 0.05};
			net.setNodeDefinition("Vectors", aVectorDef);
			double[] aQ1Def = {0.03, 0.03, 0.9, 0.04, 0.25, 0.25, 0.25, 0.25};
			net.setNodeDefinition("Q1_V", aQ1Def);
			
			/*Change nodes' attributes */			
			net.setNodePosition("Success", 20, 20, 80, 30);
			net.setNodeBgColor("Success", Color.red);
			net.setNodeTextColor("Success", Color.white);
			net.setNodeBorderColor("Success", Color.black);
			net.setNodeBorderWidth("Success", 2);
			net.setNodePosition("Forecast", 30, 100, 60, 30);
			net.writeFile("concept_inventory_link_test.xdsl");
		} catch (SMILEException ee) {
			ee.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		String input = "E:/workspace/projects/jsmile_m/concept_inventory_input_with_link.txt";
		ModelBuilder mynet = new ModelBuilder();
		
		mynet.doRead(input);
		mynet.setProb();  
		
		String output = "ci_with_link_0.7_0.8_conditional_0.5.xdsl";
		mynet.writeFile(output);
		System.out.println("File complete, closing");
	}
}
