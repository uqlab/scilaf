/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

import java.util.Random;
import scilaf.utils.CSVInstance;
import smile.Network;

public class LikelihoodValidation {
	final int NUM_OF_QUESTIONS = 27; // for all concepts
	final int NUM_OF_STUDENTS_REAL = 82; // for real data
	final int NUM_OF_STUDENTS_SYNTHETIC = 1000;  // for synthetic data
	final static int NUM_OF_OPTIONS = 5;
	final String[] OPTIONS = new String[]{"a", "b", "c", "d", "e"};
	
	public LikelihoodValidation() {}
	
	/* Generate a random answer */
	public String getRandomAnswer(){
		String answer = "";
		Random random = new Random();
		int idx = random.nextInt(5);
		answer = OPTIONS[idx];
		return answer;
	}
	
	/* Generate a csv file with random answers */
	public void generateSyntheticData(String filename){
		String[][] data = new String[NUM_OF_STUDENTS_SYNTHETIC][NUM_OF_QUESTIONS+1];
		for(int i=0; i<data.length; i++){
			data[i][0] = "RandomS"+(i+1);
			for(int j=1; j<data[0].length; j++)
				data[i][j] = getRandomAnswer();
		}
		String[] colNames = new String[data[0].length];
		colNames[0] = "name";
		for(int i=1; i<data[0].length; i++)
			colNames[i] = "q_q"+i;
		CSVInstance acsv = new CSVInstance();
		acsv.export(data, colNames, filename);
	}

	/* Estimate marginal probabilities with the first approach */
	public double[][] estimateLikelihood1(int num_of_students, Network net, String[][] data){
		int idx;
		String nodeId;
		String qname;
		double[] mpds;
		double[][] likelihood = new double[num_of_students][NUM_OF_QUESTIONS];
		for(int i=0; i<NUM_OF_QUESTIONS; i++)
			for(int j=0; j<num_of_students; j++){
				net.clearAllEvidence();
				net.updateBeliefs();
				for(int k=0; k<NUM_OF_QUESTIONS; k++){
					// set evidence for the network except the specified question
					if(k!=i) {  
						nodeId = "q_q" + (k+1);
						// data[j][k+1] refers the student j for the question k, and
						// use k+1 to skip the student name column
						if (data[j][k+1] != null && data[j][k+1].trim().length()!=0)
							net.setEvidence(nodeId, data[j][k+1]); 
					}
				}
				net.updateBeliefs();
				// estimate predicted likelihood for Qi
				qname = "q_q" + (i+1); 
				// marginal probability distribution for options a-e
				mpds = net.getNodeValue(qname);  
				if(data[j][i+1] == null || data[j][i+1].trim().length()==0)  
					likelihood[j][i] = -1;
				else {
					// identify the correct answer
					idx = data[j][i+1].charAt(0) - 'a';  
					likelihood[j][i] = mpds[idx];
				}
			}
		return likelihood;
	}

	/* Estimate marginal probabilities with the second approach */
	public String[][] estimateLikelihood2(int num_of_students, Network net, String[][] data){
		int idx;
		String nodeId;
		String qname;
		String random_answer;
		double[] mpds;
		String[][] likelihood = new String[num_of_students][2*NUM_OF_QUESTIONS];
		for(int i=0; i<NUM_OF_QUESTIONS; i++)
			for(int j=0; j<num_of_students; j++){
				net.clearAllEvidence();
				net.updateBeliefs();
				for(int k=0; k<NUM_OF_QUESTIONS; k++){
					// set evidence for the network except the specified question
					if(k!=i) {  
						nodeId = "q_q" + (k+1);
						// data[j][k+1] refers the student j for the question k; 
						// use k+1 to skip the student name column
						if (data[j][k+1] != null && data[j][k+1].trim().length()!=0)
							net.setEvidence(nodeId, data[j][k+1]); 
					}
				}
				net.updateBeliefs();
				// estimate predicted likelihood for Qi
				qname = "q_q" + (i+1); 
				// marginal probability distribution for options a-e
				mpds = net.getNodeValue(qname);  
				if(data[j][i+1] == null || data[j][i+1].trim().length()==0) 
					likelihood[j][i] = "-1";
				else {
					random_answer = getRandomAnswer();
					// identify the correct answer
					idx = random_answer.charAt(0) - 'a';  
					likelihood[j][i] = "" + mpds[idx];
					likelihood[j][i+NUM_OF_QUESTIONS] = random_answer; 
				}
			}
		return likelihood;
	}
	
	/* Estimate marginal probabilities with the third approach */
	public String[][] estimateLikelihood3(
		int num_of_students, 
		Network net, 
		String[][] data, 
		int qid){
		
		int idx;
		double[] mpds;
		String nodeId;
		String qname;
		String actual_answer;
		String random_answer;
		String[][] likelihood = new String[num_of_students][NUM_OF_OPTIONS+2];
		
		for(int j=0; j<num_of_students; j++){
			net.clearAllEvidence();
			net.updateBeliefs();
			for(int k=1; k<=NUM_OF_QUESTIONS; k++){
				if(k!=qid) {  
					// set evidence for the network except the specified 
					// question, qid refers the index of the question
					nodeId = "q_q" + k;
					// data[j][k] refers the student j for the question k;
					if(data[j][k] != null && data[j][k].trim().length()!=0)
						net.setEvidence(nodeId, data[j][k]); 
				}
			}
			net.updateBeliefs();
			// estimate predicted likelihood for Qi
			qname = "q_q" + qid; 
			// marginal probability distribution for options a-e
			mpds = net.getNodeValue(qname);
			// identify the correct answer
			actual_answer = data[j][qid];
			for(int i=0; i<NUM_OF_OPTIONS; i++){
				likelihood[j][i] = "" + mpds[i];	
			}
			likelihood[j][NUM_OF_OPTIONS] = actual_answer;
			
			if(data[j][qid] == null || data[j][qid].trim().length()==0) {
				likelihood[j][NUM_OF_OPTIONS+1] = "-1";
			} else {
				idx = actual_answer.charAt(0) - 'a';  
				likelihood[j][NUM_OF_OPTIONS+1] = "" + mpds[idx];
			}
		}
		return likelihood;
	}
}
