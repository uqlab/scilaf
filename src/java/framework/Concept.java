/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

import java.awt.Color;

public class Concept {
	public String name;
	public double prior;
	public double posterior;
	Color color;
	
	public Concept(String name){
		this.name = name;
		this.prior = 0;
		this.posterior = 0;
		this.color = Color.red;
	}
	
	/* Set prior for this concept */
	public void setPrior(double prior){
		this.prior = prior;
	}
	
	/* Set posterior for this concept */
	public void setPosterior(double posterior){
		this.posterior = posterior;
	}
	
	/* Set color for this concept */
	public void setColor(Color color){
		this.color = color;
	}
	
	/* Get prior for this concept */
	public double getPrior(){
		return this.prior;
	}
	
	/* Get posterior for this concept */
	public double getPosterior(){
		return this.posterior;
	}
	
	/* Get name for this concept */
	public String getName(){
		return this.name;
	}
	
	/* Get color for this concept */
	public Color getColor(){
		return this.color;
	}
}
