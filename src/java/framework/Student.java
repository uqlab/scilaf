/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.framework;

public class Student {
	public String username = null;
	public String group_type = null;
	public Concept[] concepts;
	public Question[] questions;
	
	public Student(){}
	
	public Student(String name) {
		this.username = name;
	}
	
	public void setUserName(String name){
		this.username = name;
	}
	
	public void setGroupType(String gtype) {
		this.group_type = gtype;
	}	

	/* Set concepts for each student instance */
	public void setConcepts(String[] cnames){
		concepts = new Concept[cnames.length];
		for(int i=0; i<cnames.length; i++) 
			this.concepts[i] = new Concept(cnames[i]);
	}
	
	/* Set questions for each student instance */
	public void setQuestions(Question[] questions){
		this.questions = questions;
	}
	
	/* Get concepts for each student instance */
	public Concept[] getConcepts(){
		return this.concepts;
	}

	/* Get questions for each student instance */
	public Question[] getQuestions(){
		return this.questions;
	}
	
	/* Update concepts for each student instance */
	public void updateConcepts(Concept[] concepts){
		this.concepts = concepts;
	}
}
