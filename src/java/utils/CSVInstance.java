/*
 * Copyright (C) 2017 Chao Chen - chen288@email.sc.edu
 *
 * This is an application framework for estimating student knowledge, 
 * identification of misconception, and evaluation of questions design 
 * as presented in the following paper:
 * 
 * Chao Chen, Ramin Madarshahian, Juan Caicedo, Charles Pierce, 
 * and Gabriel Terejanu. Assessment Framework of Student Knowledge 
 * using Bayesian Networks. to be submitted to Education and
 * Computers.
 * 
 * The framework is built on top of a graphical probabilistic library jSMILE
 * (Java interface of Structural Modeling, Inference, and Learning Engine).
 * 
 * This program is a free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, please contact the author.
 */

package scilaf.utils;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

import scilaf.framework.Question;
import scilaf.framework.Student;
import smile.learning.DataSet;

public class CSVInstance {

	public CSVInstance() {}
	
	public void export(
		List<Student> students, 
		DataSet ds, 
		int[] question_idx, 
		String[] concept_names, 
		String filename){
		
		int num_rows;
		// name + questions + concept.prior + concept.posterior
		int num_cols = question_idx.length + concept_names.length*2 + 1;  
		String[][] data = new String[students.size()][num_cols];
		String[] colnames = new String[num_cols];
		FileWriter writer;
		Student student;
		
		for(int i=0; i<students.size(); i++){
			student = students.get(i);
			data[i][0] = student.username;
			int j=0;
			for(; j<student.questions.length; j++)
				data[i][j+1] = student.questions[j].aws;
			
			for(int k=0; k<student.concepts.length; k++){
				data[i][j+2*k+1] = "" + student.concepts[k].prior;  
				data[i][j+2*k+2] = "" + student.concepts[k].posterior;  
			}
		}
		colnames[0] = "sid";
		
		/*append question names to columns names*/
		for(int i=0; i< question_idx.length; i++)
			colnames[i+1] = ds.getVariableId(question_idx[i]);
		for(int i=0; i<concept_names.length; i++){	
			colnames[question_idx.length+2*i+1] = concept_names[i]+"_prior";
			colnames[question_idx.length+2*i+2] = concept_names[i]+"_posterior";
		}
		
		try{
			writer = new FileWriter(filename);
			for (int i =0; i < colnames.length; i++){
				 writer.append(colnames[i]);
				 if (i != colnames.length - 1)
					 writer.append(",");
			}
			writer.append("\n");
			num_rows = data.length;
			num_cols = data[0].length;			 
			for (int row = 0; row < num_rows; row++)
				 for (int col = 0; col < num_cols; col++) {
					 if (col == (num_cols - 1))
						 writer.append("" + data[row][col] + "\n");
					 else {
						 writer.append("" + data[row][col] + ",");
					 }	 
				 }
			writer.flush();
			writer.close();
		} catch (Exception ee) {
			 ee.printStackTrace();
		} finally {
			 System.out.println("finish writing the result~~");
	    }
	}
	
	public void export(String[][] data, String[] colnames, String filename){
		int num_rows;
		// name + questions + concept.prior + concept.posterior + correct cnt + random questions
		int num_cols = colnames.length;  
		FileWriter writer;
		try{
			writer = new FileWriter(filename);
			for (int i =0; i < colnames.length; i++){
				 writer.append(colnames[i]);
				 if (i != colnames.length - 1)
					 writer.append(",");
			}
			writer.append("\n");
			num_rows = data.length;
			num_cols = data[0].length;			 
			for (int row = 0; row < num_rows; row++)
				 for (int col = 0; col < num_cols; col++) {
					 if (col == (num_cols - 1))
						 writer.append(data[row][col] + "\n");
					 else {
						 writer.append(data[row][col] + ",");
					 }	 
				 }
			writer.flush();
			writer.close();
		} catch (Exception ee) {
			 ee.printStackTrace();
		} finally {
			 System.out.println("finish writing the result~~");
	    }
	}
	
	public void export(double[][] data, String[] colnames, String filename){
		int num_rows;
		// name + questions + concept.prior + concept.posterior + correct cnt + random questions
		int num_cols = colnames.length;  
		FileWriter writer;
		try{
			writer = new FileWriter(filename);
			for (int i =0; i < colnames.length; i++){
				 writer.append(colnames[i]);
				 if (i != colnames.length - 1)
					 writer.append(",");
			}
			writer.append("\n");
			num_rows = data.length;
			num_cols = data[0].length;			 
			for (int row = 0; row < num_rows; row++)
				 for (int col = 0; col < num_cols; col++) {
					 if (col == (num_cols - 1))
						 writer.append("" + data[row][col] + "\n");
					 else {
						 writer.append("" + data[row][col] + ",");
					 }	 
				 }
			writer.flush();
			writer.close();
		} catch (Exception ee) {
			 ee.printStackTrace();
		} finally {
			 System.out.println("finish writing the result~~");
	    }
	}
	
	public String[][] readCSV(String file){
        CSVReader reader = null;
        List<String[]> lists = null;
        String[][] data = new String[1][1];
        
        try {
            reader = new CSVReader(new FileReader(file), ',', '"', 1); 
            lists = reader.readAll();
            reader.close();
        } catch (Exception e){
        	e.printStackTrace();
        } 
        
		try {
			if(lists==null || lists.size()==0)
				throw new Exception("the data cannot be empty!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        data = new String[lists.size()][lists.get(0).length];
        for(int row=0; row<lists.size(); row++){
        	for(int col=0; col<lists.get(0).length; col++)
        		data[row][col] = lists.get(row)[col];
        	if(data[row][1].length()==0)
				System.out.println("an empty cell occurs!!!!");
        }	
        return data;
	}
}
